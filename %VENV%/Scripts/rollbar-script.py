#!c:\%venv%\scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'rollbar==0.13.11','console_scripts','rollbar'
__requires__ = 'rollbar==0.13.11'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('rollbar==0.13.11', 'console_scripts', 'rollbar')()
    )
